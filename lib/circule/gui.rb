# frozen_string_literal: true

require_relative '../circule'

require 'digest'
require 'tempfile'

begin
  require 'glimmer-dsl-tk'
  require 'glimmer/data_binding/observer'
rescue LoadError
  warn "Could not find gem 'glimmer-dsl-tk' in locally installed gems."
end

# Glimmer Tk GUI for Circule
class Circule::GUI
  include Glimmer

  attr_accessor :hex, :bit0, :step, :bitn, :canvas, :ox, :oy, :scale

  def initialize
    initialize_defaults
  end

  def initialize_defaults
    @hex    = ARGV[0] || SecureRandom.hex(32)
    @bit0   = 0
    @step   = 16
    @bitn   = 255
    @canvas = 24
    @ox     = 0
    @oy     = 0
    @scale  = 1
  end

  def observe_image_attribute_changes
    observer = Glimmer::DataBinding::Observer.proc { generate_image }
    observer.observe(self, :hex)
    observer.observe(self, :bit0)
    observer.observe(self, :step)
    observer.observe(self, :bitn)
    observer.observe(self, :canvas)
    observer.observe(self, :ox)
    observer.observe(self, :oy)
    observer.observe(self, :scale)
  end

  def generate_image
    tfile = Tempfile.new(%w[circule- .png])

    image = Circule.new(
      hex:    @hex,
      bit0:   @bit0.to_i,
      step:   @step.to_i,
      bitn:   @bitn.to_i,
      canvas: @canvas.to_i,
      ox:     @ox.to_i,
      oy:     @oy.to_i,
    ).image

    width  = image.width * @scale.to_i
    height = image.height * @scale.to_i

    image.resize(width, height).save(tfile.path)

    @image_label.image = tfile.path
  end

  def create_gui
    @root ||= root {
      title 'Circule GUI'

      scrollbar_frame {
        xscrollbar false

        entry   {                   text <=> [self, :hex]     ; grid row:  1, column: 1, column_span: 2, column_weight: 1 }

        label   {                   text            'bit0:'   ; grid row:  2, column: 1, column_span: 1, column_weight: 0 }
        spinbox { from   0; to 255; text <=> [self, :bit0]    ; grid row:  2, column: 2, column_span: 1, column_weight: 1 }
        label   {                   text            'step:'   ; grid row:  3, column: 1, column_span: 1, column_weight: 0 }
        spinbox { from   4; to  64; text <=> [self, :step]    ; grid row:  3, column: 2, column_span: 1, column_weight: 1 }
        label   {                   text            'bitn:'   ; grid row:  4, column: 1, column_span: 1, column_weight: 0 }
        spinbox { from   0; to 255; text <=> [self, :bitn]    ; grid row:  4, column: 2, column_span: 1, column_weight: 1 }
        label   {                   text            'canvas:' ; grid row:  5, column: 1, column_span: 1, column_weight: 0 }
        spinbox { from  24; to  96; text <=> [self, :canvas]  ; grid row:  5, column: 2, column_span: 1, column_weight: 1 }
        label   {                   text            'ox:'     ; grid row:  6, column: 1, column_span: 1, column_weight: 0 }
        spinbox { from -96; to  96; text <=> [self, :ox]      ; grid row:  6, column: 2, column_span: 1, column_weight: 1 }
        label   {                   text            'oy:'     ; grid row:  7, column: 1, column_span: 1, column_weight: 0 }
        spinbox { from -96; to  96; text <=> [self, :oy]      ; grid row:  7, column: 2, column_span: 1, column_weight: 1 }
        label   {                   text            'scale:'  ; grid row:  8, column: 1, column_span: 1, column_weight: 0 }
        spinbox { from   1; to  24; text <=> [self, :scale]   ; grid row:  8, column: 2, column_span: 1, column_weight: 1 }

        frame {
          @image_label = label { anchor 'center' }            ; grid row:  9, column: 1, column_span: 2, column_weight: 1
        }

        button { text 'Open'; command { open    }             ; grid row: 10, column: 1, column_span: 2, column_weight: 1 }
        button { text 'Save'; command { save    }             ; grid row: 11, column: 1, column_span: 2, column_weight: 1 }
        button { text 'Exit'; command { exit(0) }             ; grid row: 12, column: 1, column_span: 2, column_weight: 1 }
      }
    }
  end

  def open(source = nil)
    source   ||= send(:get_open_file, parent: root)
    self.hex   = Digest::SHA256.hexdigest File.read(source)
  end

  def save(target = nil)
    source   = @image_label.image
    target ||= send(:get_save_file, parent: @root)
    source.write(target)
  end

  def launch
    observe_image_attribute_changes
    create_gui
    generate_image
    @root.open
  end
end
