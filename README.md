# Circule

Generate an icon of overlapping circles derived from a hash.

1. Slice the hash into 16-bit slices starting at bit `bn`, stepping `s`
   bits at a time, ending after bit `bn`.

2. From each slice define a circle such that bits 0 through 4 is the `x`
   coordinate of the center, bits 5 through 9 is the `y` coordinate of
   the center, and bits 10 through 15 is the radius of the circle.

3. Each pixel in the output will be colored based on the number of
   circles that point is inside.

The [demo video](circule-demo.mp4) shows how the final image is composed.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'circule'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install circule

## Usage

### CLI Options

| Short Option | Long Option   | Description                      |
|--------------|---------------|----------------------------------|
| -h HEXSTRING | --hex         | Use this hash                    |
| -b INTEGER   | --btc-block   | Use hash from this bitcoin block |
| -0 INTEGER   | --bit0        | Starting bit                     |
| -s INTEGER   | --step        | Step bit                         |
| -n INTEGER   | --bitn        | Starting bit                     |
| -c INTEGER   | --canvas      | Canvas size                      |
| -x INTEGER   | --ox          | Offset x                         |
| -y INTEGER   | --oy          | Offset y                         |
| -z INTEGER   | --zoom        | Zoom in GUI                      |
| -f FILENAME  | --file        | Use hash from file               |
| -s FILENAME  | --save        | Save image to file               |
| -g           | --gui         | Run GUI (requires glimmer & tk)  |
| -i           | --interactive | Run GUI with pry                 |

### CLI Example(s)

The icon for this project on GitLab:
```
$ echo -n circule | sha256sum
4b9ae43c4e15486ce131f733eff14547764623d27aa5d1d9aff45b67528f0b60  -

$ circule -h 4b9ae43c4e15486ce131f733eff14547764623d27aa5d1d9aff45b67528f0b60 -s /tmp/circule.png
```

## Development

After checking out the repo, run `bin/setup` to install
dependencies. Then, run `rake test` to run the tests. You can also run
`bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake
install`. To release a new version, update the version number in
`version.rb`, and then run `bundle exec rake release`, which will create
a git tag for the version, push git commits and the created tag, and
push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at
https://gitlab.com/fjc/circule.

## License

The gem is available as open source under the terms of the [MIT
License](https://opensource.org/licenses/MIT).
