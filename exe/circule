#!/usr/bin/env ruby
# frozen_string_literal: true

require 'circule'
require 'optparse'

options = {}
OptionParser.new do |opts|
  opts.banner = 'Generate an icon of overlapping circles derived from a hash.'

  opts.on('-h HEXSTRING', '--hex', 'Use this hash') do |h|
    options[:hex] = h
  end

  opts.on('-b INTEGER', '--btc-block', 'Use hash from this bitcoin block') do |b|
    options[:btc_block] = b
  end

  opts.on('-0 INTEGER', '--bit0', 'Starting bit') do |b|
    options[:bit0] = b
  end

  opts.on('-s INTEGER', '--step', 'Step bit') do |s|
    options[:step] = s
  end

  opts.on('-n INTEGER', '--bitn', 'Starting bit') do |b|
    options[:bitn] = b
  end

  opts.on('-c INTEGER', '--canvas', 'Canvas size') do |c|
    options[:canvas] = c
  end

  opts.on('-x INTEGER', '--ox', 'Offset x') do |x|
    options[:ox] = x
  end

  opts.on('-y INTEGER', '--oy', 'Offset y') do |y|
    options[:oy] = y
  end

  opts.on('-z INTEGER', '--zoom', 'Zoom in GUI') do |z|
    options[:zoom] = z
  end

  opts.on('-f FILENAME', '--file', 'Use hash from file') do |f|
    options[:file] = f
  end

  opts.on('-s FILENAME', '--save', 'Save image to file') do |s|
    options[:save] = s
  end

  opts.on('-g', '--gui', 'Run GUI') do |g|
    options[:gui] = g
  end

  opts.on('-i', '--interactive', 'Run GUI with pry') do |i|
    options[:interactive] = i
  end
end.parse!

if options[:gui]
  require 'circule/gui'
  gui = Circule::GUI.new

  gui.hex       = options[:hex]    if options[:hex]
  gui.bit0      = options[:bit0]   if options[:bit0]
  gui.step      = options[:step]   if options[:step]
  gui.bitn      = options[:bitn]   if options[:bitn]
  gui.canvas    = options[:canvas] if options[:canvas]
  gui.ox        = options[:ox]     if options[:ox]
  gui.oy        = options[:oy]     if options[:oy]
  gui.scale     = options[:zoom]   if options[:zoom]

  gui.open options[:file] if options[:file]

  if options[:save]
    gui.create_gui
    gui.generate_image
    gui.save options[:save]
  end

  if options[:interactive]
    require 'pry'
    require 'trace_eval' rescue nil
    Thread.new do gui.pry ensure `reset` end
  end

  gui.launch
else
  case options[:file]
  when nil
  when '-'
    require 'digest'
    options[:hex] = Digest::SHA256.hexdigest $stdin.read
  else
    require 'digest'
    options[:hex] = Digest::SHA256.hexdigest File.read(options[:file])
  end

  Circule.new(
    **{
        hex:       options[:hex],
        btc_block: options[:btc_block],
        bit0:      options[:bit0],
        step:      options[:step],
        bitn:      options[:bitn],
        canvas:    options[:canvas],
        ox:        options[:ox],
        oy:        options[:oy],
      }.compact
  ).tap do |circule|
    case options[:save]
    when nil
    when '-'
      circule.image.write $stdout
    else
      circule.image.save options[:save]
    end
  end
end
